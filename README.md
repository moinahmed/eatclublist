# EatClubList

Coding task is to replicate EatClub's restaurants list.

1. Generate own JSON data.
2. Design restaurants list screen.
3. Feed dummy data into restaurant list.

---

## Steps to run the project.

1. Install Android Studio, at-least V3.4.
2. Install Android SDK V28.
3. Make sure buildToolsVersion "29.0.0" is intalled.
4. Clone this project.
5. Open project in Android Studio.
6. Build and run the project. (make sure gradle sync is completed)

---

## Libraries used:

Gradle will automatically sync and install them.

1. **Androidx** - for backward compatibility and support previous android versions.
2. **Gson** - to parse json dummy data.
3. **Recycler View** - to render list efficiently.
4. **Card View** - to show card layout with corner radius.

---

## Output

![Image](screenshots/output.png)

---

## Limitations and future tasks

1. Data structure was not clear at the time of design. Design proper data structure and change code accordingly.
2. Integrate Automated testing.
3. Improve XML layout structure by dividing into smaller components.
4. Improve Recycler View -> Adapter to render "Offers" efficiently.
5. Add error handling for corner cases.
6. Effecient rendering of background image. As of now just a default image is selected. (Planning to implement network image cache).

---

## Author

* Moin Ahmed - moinahmed17@gmail.com