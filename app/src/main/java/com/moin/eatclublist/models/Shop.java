package com.moin.eatclublist.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * model class for shop object
 */

public class Shop {
    private String name;
    private String distance;
    private String tags;
    @SerializedName("is_favourite")
    private boolean isFavourite;
    @SerializedName("is_takeaway")
    private boolean isTakeaway;
    @SerializedName("is_new")
    private boolean isNew;
    private List<Offer> offers;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public boolean isTakeaway() {
        return isTakeaway;
    }

    public void setTakeaway(boolean takeaway) {
        isTakeaway = takeaway;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }
}
