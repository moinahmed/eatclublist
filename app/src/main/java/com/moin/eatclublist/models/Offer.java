package com.moin.eatclublist.models;

public class Offer {
    private String label;
    private String expiry;
    private String preBook;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getPreBook() {
        return preBook;
    }

    public void setPreBook(String preBook) {
        this.preBook = preBook;
    }
}
