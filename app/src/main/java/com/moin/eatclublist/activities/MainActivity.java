package com.moin.eatclublist.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.moin.eatclublist.R;
import com.moin.eatclublist.adapters.ShopListAdapter;
import com.moin.eatclublist.models.Shop;
import com.moin.eatclublist.utils.Helper;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView listRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private Gson gson; // Json library reference to parse json response.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gson = Helper.getGsonInstance();

        setupListView();

        fetchData();
    }

    /**
     * initialise recycler view with default config.
     */
    private void setupListView(){
        listRecyclerView = findViewById(R.id.list_recycler_view);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        listRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        listRecyclerView.setLayoutManager(layoutManager);
    }

    /**
     * Fetches data from json and render into recycler view
     */
    private void fetchData(){
        JSONArray data = Helper.getJSONData();
        Type listType = new TypeToken<List<Shop>>() {}.getType();
        List<Shop> shopList = gson.fromJson(data.toString(), listType);
        ShopListAdapter adapter = new ShopListAdapter(this, shopList);
        listRecyclerView.setAdapter(adapter);
    }
}
