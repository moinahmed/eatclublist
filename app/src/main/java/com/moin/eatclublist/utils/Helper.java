package com.moin.eatclublist.utils;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class Helper {

    /**
     * Dummy method to generate shop lists
     * @return JSONArray of shop lists
     */
    public static JSONArray getJSONData(){
        JSONArray data = new JSONArray();
        try{
            for(int i=0; i<15; i++){
                JSONObject obj = new JSONObject();
                obj.put("name","Shop Name "+i);
                obj.put("distance",i*10+"km Away");
                obj.put("tags","Asian, Thai, Indian");
                obj.put("is_favourite",(i%2 == 0)? true: false);
                obj.put("is_takeaway",(i%2 == 0)? true: false);
                obj.put("is_new",(i%2 == 0)? true: false);
                obj.put("offers", generateOffers());
                data.put(obj);
            }
        }catch (JSONException e){ }
        return data;
    }

    /**
     * dummy method to generate offers for each shop
     * @return JSONArray of offers
     */
    public static JSONArray generateOffers(){
        JSONArray offersArray = new JSONArray();
        try {
            int random = (int )(Math.random() * 3 + 0);
            while (random > 0){
                JSONObject offer = new JSONObject();
                offer.put("label", "10$ off");
                offer.put("expiry", "1h 20m");
                offer.put("pre_book", "3:01pm - 6:01pm");
                offersArray.put(offer);
                random --;
            }
        }catch (JSONException e){ }
        return offersArray;
    }

    /**
     * Create a gson library instance and return with custom config.
     * @return Gson library instance
     */
    public static Gson getGsonInstance(){
        return new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .registerTypeAdapter(boolean.class, Helper.booleanAsIntAdapter)
                .create();
    }

    /**
     * Helper for parsing int to boolean values from json
     * It will be used in GSON config.
     */
    public static final TypeAdapter<Boolean> booleanAsIntAdapter = new TypeAdapter<Boolean>() {
        @Override public void write(JsonWriter out, Boolean value) throws IOException {
            if (value == null) {
                out.nullValue();
            } else {
                out.value(value);
            }
        }
        @Override public Boolean read(JsonReader in) throws IOException {
            JsonToken peek = in.peek();
            switch (peek) {
                case BOOLEAN:
                    return in.nextBoolean();
                case NULL:
                    in.nextNull();
                    return null;
                case NUMBER:
                    return in.nextInt() != 0;
                case STRING:
                    return in.nextString().equalsIgnoreCase("1");
                default:
                    throw new IllegalStateException("Expected BOOLEAN or NUMBER but was " + peek);
            }
        }
    };
}
