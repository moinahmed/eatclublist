package com.moin.eatclublist.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.moin.eatclublist.R;
import com.moin.eatclublist.models.Offer;
import com.moin.eatclublist.models.Shop;

import java.util.List;

public class ShopListAdapter extends RecyclerView.Adapter<ShopListAdapter.ShopViewHolder> {
    private List<Shop> mDataset;
    private Context ct;
    LayoutInflater inflater;

    public ShopListAdapter(Context ct, List<Shop> data){
        mDataset = data;
        inflater = LayoutInflater.from(ct);
    }

    //method to replace old data with new one
    public void replaceData(List<Shop> data){
        mDataset.clear();
        mDataset.addAll(data);
        notifyDataSetChanged();
    }

    public static class ShopViewHolder extends RecyclerView.ViewHolder{
        private ImageView bgImage;
        private TextView shopName;
        private TextView distance;
        private TextView takeAway;
        private TextView tags;
        private LinearLayout newBadge;
        private LinearLayout offersContainer;
        private ImageView favourite;

        public ShopViewHolder(@NonNull View v) {
            super(v);
            bgImage = v.findViewById(R.id.bg_image);
            shopName = v.findViewById(R.id.shop_name);
            distance = v.findViewById(R.id.distance);
            takeAway = v.findViewById(R.id.take_away);
            tags = v.findViewById(R.id.tags);
            newBadge = v.findViewById(R.id.new_badge);
            offersContainer = v.findViewById(R.id.offers_container);
            favourite = v.findViewById(R.id.favourite);
        }
    }

    @NonNull
    @Override
    public ShopListAdapter.ShopViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowLayout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.shop_row, parent, false);

        ShopViewHolder fh = new ShopViewHolder(rowLayout);
        return fh;
    }

    @Override
    public void onBindViewHolder(@NonNull ShopListAdapter.ShopViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Shop shop = mDataset.get(position);
        holder.shopName.setText(shop.getName());
        holder.distance.setText(shop.getDistance());
        holder.tags.setText(shop.getTags());

        holder.takeAway.setVisibility(View.INVISIBLE);
        if (shop.isTakeaway())
            holder.takeAway.setVisibility(View.VISIBLE);

        holder.newBadge.setVisibility(View.INVISIBLE);
        if (shop.isNew())
            holder.newBadge.setVisibility(View.VISIBLE);

        //render services
        holder.offersContainer.removeAllViews();
        for(Offer offer : shop.getOffers()){
            View v = inflater.inflate(R.layout.offer_layout, holder.offersContainer, false);
            ((TextView)v.findViewById(R.id.titleTxt)).setText(offer.getLabel());
            holder.offersContainer.addView(v);
        }

        //TODO handle favourite
        if (shop.isFavourite()){

        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
